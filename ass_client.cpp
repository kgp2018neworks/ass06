/* 
 * udpclient.c - A simple UDP client
 * usage: udpclient <host> <port>
 */
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/shm.h>


#define BUFSIZE 1024

using namespace std;

int win = 3;

#define MAX 1000

/* 
 * error - wrapper for perror
 */


struct send_pack 
{
	int size;
	char pk[BUFSIZE];
	int seq;
};




int max(int a, int b)
{
	if (a>=b)
		return a;
	else return b;
}
void error(char *msg) {
    perror(msg);
    exit(0);
}
int rec_calc(char buf[])
{
	char cbuf[10];
    int testc;
    sscanf(buf,"%s %04d", cbuf, &testc);

    return testc;
}

//Checks for correct acknowledgemnet
void check_ack(char buf[])
{
    char cbuf[10];
    int testc;
    sscanf(buf,"%s %04d", cbuf, &testc);
    //printf("%d\n", testc);

    printf("Acknowledgement for packet no. %d received\n", testc);
    //else return 0;
}
void check_send(char buf[])
{
    //char cbuf[10];
    int seq;
    sscanf(buf,"%04d", &seq);
    //printf("%d\n", testc);

    printf("Packet no. %d sent\n", seq);
    //else return 0;
}

int main(int argc, char **argv) {
    int sockfd, portno, n;
    int serverlen;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    char buf[BUFSIZE];
    char rec_buf[BUFSIZE];
    char temp_buf[BUFSIZE-8];
    fd_set readfds;
    struct timeval tv;
    

    if (argc != 4) {
       fprintf(stderr,"usage: %s <hostname> <port> <filename>\n", argv[0]);
       exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", hostname);
        exit(0);
    }

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);
    serverlen = sizeof(serveraddr);
    /* get a message from the user */
    //printf("Round 0 compn\n");
    bzero(buf, BUFSIZE);    
    FILE *filefd = fopen(argv[3], "rb");

    /*struct stat st;
    fstat(filefd, &st);
    int file_size = st.st_size;*/
    int file_size;
    fseek(filefd, 0, SEEK_END);
    file_size = ftell(filefd);
    rewind(filefd);
    int size = file_size,chunk=0;

    //send_pack temp[1000];

    /*Calculation of total chunks*/
    while(size>0)
    {
        chunk++;
        size-=(BUFSIZE-8);
    }
    //printf("%d\n", file_size);
    
    /*Sending File descriptors*/
    sprintf(buf,"Filename: %s File_size: %d Total Chunks: %d", argv[3], file_size, chunk);
    printf("Filename: %s of filesize: %d being sent...\n", argv[3], file_size);
    
    //printf("buffer : %s\n", buf);
    //printf("Round 1/2 compn %d\n", chunk);
    
    n = sendto(sockfd, buf, strlen(buf), 0, (const sockaddr *)&serveraddr, serverlen);
    if (n < 0) 
      error("ERROR in sendto");
    
    //printf("Round 2/3 compn\n");
    bzero(buf, BUFSIZE);
    
    n = recvfrom(sockfd, rec_buf, BUFSIZE, 0, (sockaddr *)&serveraddr, (socklen_t *)&serverlen);
    if (n < 0) 
      error("ERROR in recvfrom");
    
    int read_returns,flag,k,rec, seq_no, chunk_size;
    int var, init;
    //printf("Round 1 compn\n");
    int count = 0, counter =0,csend,i,cg, prev_win;
    bzero(buf, BUFSIZE);

    vector<send_pack> unique;
    send_pack tempo;
	tempo.size = read_returns;
	tempo.seq = count;
	memcpy(tempo.pk, buf, read_returns);
    unique.push_back(tempo);

    //int shmid = shmget(IPC_PRIVATE, 4*sizeof(int), 0777|IPC_CREAT);
    pid_t pid;
    //pid = fork();

    //if(pid != 0)
    //{   
    	int baseptr = 1, currptr = 1;
    	int b[5];
    	//b = (int *) shmat(shmid, 0, 0);
    	b[0] = 1;
    	b[1] = win;
    	b[2] = -1;
    	b[3] = 0;
    	b[4] = 0; 
    	printf("mai send\n");
    	while (1) 
    	{
    		baseptr = b[0];
            win = b[1];
        	while(currptr < baseptr + win)
        	{
        		while(unique.size() < baseptr + win  && (b[2]!=count))   //reading from file
        		{
        			bzero(buf, BUFSIZE);
	        		read_returns = fread(temp_buf,1, BUFSIZE-8, filefd);
	        		if (read_returns == -1) 
	            		error("Couldn't read from file");
	        		if (read_returns == 0) 
	        		{
	        			printf("File read over\n");
	        			b[2] = count;
	           			fclose(filefd);
	           			break;
	        		}
		        	count++;
		        	//printf("ye le count %d\n", count);
		        	//counter += read_returns;
		        	sprintf(buf, "%04d%04d", count, read_returns);
		        	memcpy(buf+8, temp_buf, read_returns);
		        	send_pack temp;
		        	temp.size = read_returns+8;
		        	temp.seq = count;
		        	memcpy(temp.pk, buf, read_returns+8);
		        	unique.push_back(temp);
        		}
        		/*for(int i=0; i<baseptr+win; i++)
        		{
        			printf("%d\n", sizeof(unique[i].pk));
        		}*/
        		printf("bp: %d cp: %d\n", baseptr, currptr);
        		bzero(buf, BUFSIZE);
        		//if(b[2] == count)
        		//printf("HAHAHAHA size of vec: %d count: %d\n", unique.size(), count);
        		if(currptr >= unique.size())
        			break;
        		memcpy(buf, unique[currptr].pk, unique[currptr].size);
        		//printf("seq: %d curr_window: %d\n",unique[currptr].seq, win);
        		check_send(buf);
        		i = sendto(sockfd, buf, unique[currptr].size , 0, (const sockaddr *)&serveraddr, serverlen);
                if (i == -1)
                	error("Couldn't write to socket");

                currptr++;

                //baseptr = b[0];
                //win = b[1];
                
        	}

        	tv.tv_sec = 1;
    		tv.tv_usec = 000000;
        	FD_ZERO(&readfds);
        	FD_SET(sockfd, &readfds);
        	select(sockfd+1, &readfds, NULL, NULL, &tv);
        	if(FD_ISSET(sockfd, &readfds))
        	{
        		//printf("mereko mila\n");
        		bzero(rec_buf, BUFSIZE);
            	n = recvfrom(sockfd, rec_buf, BUFSIZE, 0, (sockaddr *)&serveraddr, (socklen_t *)&serverlen);
	            if (n < 0) 
	            	error("ERROR in recvfrom");
	            rec = rec_calc(rec_buf);
	            check_ack(rec_buf);
	            if(rec == b[2])
	            {
	            	b[3]=1;
	            	break;
	            }
	            if(rec > b[4])
	            {
	            	b[1]+= rec - b[0] + 1;
	            	b[0] = rec + 1;
	            	b[4] = rec;
                    printf("window for next iter: %d\n", b[1]);
	            }
        		continue;
        	}
        	else
        	{
        		printf("Window size reduced\n");
        		win = max(1, win/=2);
        		currptr = baseptr;
        	}
        }
        	if(b[3] == 1)
        	{
        		printf("All files sent\n");
		        bzero(rec_buf, BUFSIZE);
		        n = recvfrom(sockfd, rec_buf, BUFSIZE, 0, (sockaddr *)&serveraddr, (socklen_t *)&serverlen);
		        if (n < 0) 
		            error("ERROR in recvfrom");
		        if (strcmp(rec_buf, "ACK_FINAL"))
		            error("Wrong Acknowledgement received 1");
		        else
		            printf("Closing connection request accepted\n");
		        bzero(buf, BUFSIZE);
		        sprintf(buf, "ACK");
		        i = sendto(sockfd, buf, BUFSIZE, 0, (sockaddr *)&serveraddr, serverlen);
		        if (i == -1)
		            error("Couldn't write to socket");
	        }
}
    //}

    /*else  //receive ack
    {
    	int *a;
    	a = (int *) shmat(shmid, 0, 0);
    	a[1] = win;
    	printf("mai recv\n");
    	int max_ack = 0;
    	while(1)
    	{	
        	bzero(rec_buf, BUFSIZE);
            n = recvfrom(sockfd, rec_buf, BUFSIZE, 0, (sockaddr *)&serveraddr, (socklen_t *)&serverlen);
            if (n < 0) 
            	error("ERROR in recvfrom");
            rec = rec_calc(rec_buf);
            check_ack(rec_buf);
            if(rec == a[2])
            {
            	a[3]=1;
            	break;
            }
            if(rec > max_ack)
            {
            	a[1]+= rec - a[0] + 1;
            	a[0] = rec + 1;
            	max_ack = rec;
            }
        }
    }*/



