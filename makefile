all: sr cl

sr: ass_server.c
	gcc ass_server.c -o sr

cl: ass_client.cpp
	g++ -w ass_client.cpp -o cl

clean:
	rm sr cl

